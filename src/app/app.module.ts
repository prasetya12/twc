import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {StoreModule } from '@ngrx/store'
import {StoreDevtoolsModule} from '@ngrx/store-devtools'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { DestinationComponent } from './pages/destination/destination.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CardComponent } from './components/card/card.component';
import { SliderCardComponent } from './components/slider-card/slider-card.component';
import { DestinantionCardComponent } from './components/destinantion-card/destinantion-card.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DetailComponent } from './pages/detail/detail.component';
import { BookingFormComponent } from './components/booking-form/booking-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {reducers} from '../app/reducers'
import { LightboxModule } from 'ngx-lightbox';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ReactiveFormsModule } from '@angular/forms';
import { CartViewComponent } from './pages/cart-view/cart-view.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { BlankLayoutComponent } from './layout/blank-layout/blank-layout.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component'
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http'
import {APIInterceptor} from './interceptors/api.interceptor'
import { productEffects } from '../app/effects/product.effects';
import { bookingEffects } from '../app/effects/booking.effects';

import { EffectsModule } from '@ngrx/effects';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ProductsComponent } from './pages/products/products.component';
import { AngularFireModule } from 'angularfire2';
import {AngularFirestore, DocumentChangeAction} from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';


import { environment } from 'src/environments/environment';
import { ScrollableDirective } from './scrollable.directive';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { NewsCardComponent } from './components/news-card/news-card.component';
import { SejarahComponent } from './pages/sejarah/sejarah.component';
import { SambutanComponent } from './pages/sambutan/sambutan.component';
import { VisiMisiComponent } from './pages/visi-misi/visi-misi.component';
import { BeritaComponent } from './pages/berita/berita.component';
import { SatwaComponent } from './pages/satwa/satwa.component';
import { WahanaPermainanComponent } from './pages/wahana-permainan/wahana-permainan.component';
import { PaketEdukasiComponent } from './pages/paket-edukasi/paket-edukasi.component';
import { FasilitasUmumComponent } from './pages/fasilitas-umum/fasilitas-umum.component';
import { GaleriFotoComponent } from './pages/galeri-foto/galeri-foto.component';
import { GaleriVideoComponent } from './pages/galeri-video/galeri-video.component';
import { News01Component } from './pages/news01/news01.component';
import { News02Component } from './pages/news02/news02.component';
import { News03Component } from './pages/news03/news03.component';
import { ProfileComponent } from './pages/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DestinationComponent, 
    HeaderComponent,
    FooterComponent,
    CardComponent,
    SliderCardComponent,
    DestinantionCardComponent,
    DetailComponent,
    BookingFormComponent,
    CartViewComponent,
    PaymentComponent,
    BlankLayoutComponent,
    MainLayoutComponent,
    PaymentSuccessComponent,
    CatalogComponent,
    ProductsComponent,
    ScrollableDirective,
    LoadingSpinnerComponent,
    NewsCardComponent,
    SejarahComponent,
    SambutanComponent,
    VisiMisiComponent,
    BeritaComponent,
    SatwaComponent,
    WahanaPermainanComponent,
    PaketEdukasiComponent,
    FasilitasUmumComponent,
    GaleriFotoComponent,
    GaleriVideoComponent,
    News01Component,
    News02Component,
    News03Component,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([productEffects,bookingEffects]),
    SlickCarouselModule,
    FormsModule,
    NgbModule,
    LightboxModule,
    SweetAlert2Module,
    ReactiveFormsModule,
    InfiniteScrollModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule


  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true,
  },AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
