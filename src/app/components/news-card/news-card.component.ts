import { Component, OnInit, Input } from '@angular/core';


@Component({
    selector: 'app-news-card',
    templateUrl: './news-card.component.html',
    styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {
    @Input() date:string;
    @Input() month:string;
    @Input() img:string;
    @Input() url:string;
    @Input() title:string;
    @Input() text:string;
    
    constructor() { }

    ngOnInit(): void {
    }

}
