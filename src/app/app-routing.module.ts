import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from '../app/pages/home/home.component'
import {DestinationComponent} from '../app/pages/destination/destination.component'
import {DetailComponent} from '../app/pages/detail/detail.component'
import {CartViewComponent} from '../app/pages/cart-view/cart-view.component'
import {PaymentComponent} from '../app/pages/payment/payment.component'
import {PaymentSuccessComponent} from '../app/pages/payment-success/payment-success.component'
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ProductsComponent } from './pages/products/products.component';
import { SejarahComponent } from './pages/sejarah/sejarah.component';
import { SambutanComponent  } from './pages/sambutan/sambutan.component';
import { VisiMisiComponent } from './pages/visi-misi/visi-misi.component'
import { WahanaPermainanComponent } from './pages/wahana-permainan/wahana-permainan.component';
import { PaketEdukasiComponent } from './pages/paket-edukasi/paket-edukasi.component';
import { FasilitasUmumComponent } from './pages/fasilitas-umum/fasilitas-umum.component';
import { GaleriFotoComponent } from './pages/galeri-foto/galeri-foto.component';
import { GaleriVideoComponent } from './pages/galeri-video/galeri-video.component';
import { BeritaComponent } from './pages/berita/berita.component';



import {AppComponent} from '../app/app.component'
import {MainLayoutComponent} from '../app/layout/main-layout/main-layout.component'

import { News01Component } from './pages/news01/news01.component'
import { News02Component } from './pages/news02/news02.component'
import { News03Component } from './pages/news03/news03.component'
import { ProfileComponent } from './pages/profile/profile.component'

const routes: Routes =
    [{
	path:'payment/success',
	component:PaymentSuccessComponent
     }
     ,{
	 path:'payment/ref/:reference',
	 component:PaymentComponent
     }
     ,{
	 path:'catalog',
	 component:CatalogComponent
     }
     ,{
	 path:'products',
	 component:ProductsComponent
     }
     
     ,{
  path:'',
  component:MainLayoutComponent,
  children:[{
      path:'',
      component:HomeComponent
  }, {
      path:'galeri',
      component:GaleriFotoComponent
  }, {
      path:'video',
      component:GaleriVideoComponent
  }, {
      path:'berita',
      component:BeritaComponent
  }, {
      path:'news01',
      component:News01Component
  }, {
      path:'news02',
      component:News02Component
  }, {
      path:'news03',
      component:News03Component
  }, {
      path:'cart/view',
      component:CartViewComponent
  }, {
      path:'profil',
      component:ProfileComponent
  }, {
      path:'profil/sejarah',
      component:SejarahComponent
  }, {
      path:'profil/sambutan-direktur',
      component:SambutanComponent
  }, {
      path:'profil/visi-misi',
      component:VisiMisiComponent
  }, {
      path:'layanan/wahana-permainan',
      component:WahanaPermainanComponent
  }, {
      path:'layanan/paket-edukasi',
      component:PaketEdukasiComponent
  }, {
      path:'layanan/fasilitas-umum',
      component:FasilitasUmumComponent
  }, {
      path:':place',
      component:DestinationComponent
  }, {
      path:':place/tiket-masuk/:id',
      component:DetailComponent
  }, {
      path:':place/activity/:id',
      component:DetailComponent
  }, {
      path:':place/tour-paket/:id',
      component:DetailComponent
  }
  ]
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
