import { ActionReducerMap } from "@ngrx/store";
import { productReducer } from "./product.reducer";
import { cartReducer } from "./cart.reducer";
import { bookingReducer } from "./booking.reducer";
import { newsReducer } from "./news.reducer";


interface AppState {
    product: any;
    cart: any;
    booking:any;
    news:any;
  }
export const reducers: ActionReducerMap<AppState> = {
    product: productReducer,
    cart: cartReducer,
    booking:bookingReducer,
    news:newsReducer
};
