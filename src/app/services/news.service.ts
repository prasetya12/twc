import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment'
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private httpClient:HttpClient ) { }

  public getNews(){
    return this.httpClient.get(environment.url_cms+'/articles')
  }
  
  

}
