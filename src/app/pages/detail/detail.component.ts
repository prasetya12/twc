import { Component, OnInit } from '@angular/core';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';
import {Router} from '@angular/router';
import {getDetailAttraction,getTicketType} from '../../actions/product.actions'
import {select, Store} from '@ngrx/store'
import {environment} from 'src/environments/environment'

import data from '../../../data/data.json'
import { Lightbox } from 'ngx-lightbox';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  path:any
  place:string
  type:string
  id:number
  detailProduct:any={};
  detail:any={};
  ticketTypes:any=[]
  private _albums:any = [];
  env:any

  constructor(location: Location,private router: Router,private _lightbox: Lightbox,private store:Store<any>) { 
    this.path = router.url
    var convert = this.path.split('/')
    this.place = convert[1]
    this.type = convert[2]
    this.id = parseInt(convert[3])
    this.env = environment.url
    if(this.place=='borobudur'){
      
      this.store.dispatch(new getDetailAttraction(this.id))
      this.store.dispatch(new getTicketType(this.id))
      // this.getTicketType()
      // this.store.dispatch(new getTicketType(this.id))


      const temp_detail =  this.store.pipe(select(state => state.product.detailProduct))
      temp_detail.subscribe(res => {
        this.detail = res
      });

      const temp_ticket =  this.store.pipe(select(state => state.product.dataTicketType))
      temp_ticket.subscribe(res => {
        this.ticketTypes = res
      });




    }else{
      this.detailProduct = data[this.place][this.type][this.id]
      this.detailProduct.image.forEach((item)=>{
        const caption = 'Image caption here';
        const album = {
          src:item,
          caption: caption,
          thumb: item
        }

        this._albums.push(album);
      })
    }
   }

  ngOnInit(): void {
  }


  open(index: number): void {
    console.log(this._albums)
    // open lightbox
    this._lightbox.open(this._albums, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  

  

}
