import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SambutanComponent } from './sambutan.component';

describe('SambutanComponent', () => {
  let component: SambutanComponent;
  let fixture: ComponentFixture<SambutanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SambutanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SambutanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
