import { Component, OnInit } from '@angular/core';
import data from '../../../data/data.json'

@Component({
    selector: 'app-berita',
    templateUrl: './berita.component.html',
    styleUrls: ['./berita.component.scss']
})
export class BeritaComponent implements OnInit {
    newscards:any;
    constructor() {
	this.newscards = data['newscards']
    }

    ngOnInit(): void {
    }

}
