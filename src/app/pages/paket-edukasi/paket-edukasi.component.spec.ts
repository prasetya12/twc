import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaketEdukasiComponent } from './paket-edukasi.component';

describe('PaketEdukasiComponent', () => {
  let component: PaketEdukasiComponent;
  let fixture: ComponentFixture<PaketEdukasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaketEdukasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaketEdukasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
