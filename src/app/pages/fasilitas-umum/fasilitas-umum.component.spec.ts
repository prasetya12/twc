import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FasilitasUmumComponent } from './fasilitas-umum.component';

describe('FasilitasUmumComponent', () => {
  let component: FasilitasUmumComponent;
  let fixture: ComponentFixture<FasilitasUmumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FasilitasUmumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FasilitasUmumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
