import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WahanaPermainanComponent } from './wahana-permainan.component';

describe('WahanaPermainanComponent', () => {
  let component: WahanaPermainanComponent;
  let fixture: ComponentFixture<WahanaPermainanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WahanaPermainanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WahanaPermainanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
