import { Component, OnInit } from '@angular/core';
import data from '../../../data/data.json'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    bundling:any;
    destinasi:any;
    newscards:any;
    slides: string[] = [ "hintrip-banner.png"
			 , "grandpremium-zone-a1.png"
			 , "grandpremium-zone-a2.png"
			 , "grandpremium-zone-a3.png"
			 , "grandpremium-zone-b1.png"
			 , "grandpremium-zone-jk1.png"
			 , "LOGO-ITDC-BARU.png"
			 , "Sarinah-Flyer.png"
		       ];
    slideConfig = {"slidesToShow": 3, "slidesToScroll": 4};
    
    constructor() { 
	this.bundling = data['products'].filter(ea => ea.category == "bundling").sort((ea1, ea2) => ea1.rank - ea2.rank)
	this.destinasi = data['products'].filter(ea => ea.category == "destinasi").sort((ea1, ea2) => ea1.rank - ea2.rank)
	this.newscards = data['newscards'].slice(0, 2)
    }

    ngOnInit(): void {
    }

    formatNumber(number){
	const format = number.toString().split('').reverse().join('');
	const convert = format.match(/\d{1,3}/g);
	const num = convert.join('.').split('').reverse().join('')
	// return number.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return num;
    }

    

}
