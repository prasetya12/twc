import { Component, OnInit,ViewChild } from '@angular/core';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';
import {Router,ActivatedRoute} from '@angular/router';
import data from '../../../data/data.json'
import { SlickCarouselComponent } from "ngx-slick-carousel";
import { Subscription } from "rxjs";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import {getAttractions,removeData,getActivities,getTours} from '../../actions/product.actions'
import {select, Store} from '@ngrx/store'
import {environment} from 'src/environments/environment'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss']
})
export class DestinationComponent implements OnInit {
  path:any
  place:string
  destination: any;
  url:any
  tiket_masuk:Observable<any>
  activities:Observable<any>
  tours:Observable<any>
  env:any

  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
  slideConfig = {"slidesToShow": 3, "slidesToScroll": 4};

  constructor(location: Location,private router: Router,private route: ActivatedRoute,private sanitizer: DomSanitizer,private store:Store<any>) {
    this.path = router.url
    var convert = this.path.split('/')
    this.place = convert[1]
    
    this.route.params.subscribe(params => {
        this.env = environment.url

        this.place = params.place
        if(this.place == 'borobudur'){
          this.destination={}
          const params = {
            countryId:2,
            excludeTicketTypes:true,
            cityId:8
          }
          this.store.dispatch(new getAttractions(params))

          this.store.dispatch(new getActivities({
            countryId:2,
            excludeTicketTypes:true,
            cityId:5,
            tab:'Lifestyle'
          }))

          this.store.dispatch(new getTours({
            countryId:2,
            excludeTicketTypes:true,
            cityId:5,
            tab:'Tours'
          }))

          this.tiket_masuk  = this.store.pipe(select(state => state.product.data))
          this.activities  = this.store.pipe(select(state => state.product.activities))
          this.tours  = this.store.pipe(select(state => state.product.tours))

  
        }else{
          this.store.dispatch(new removeData())
          this.destination= data[params.place]
          this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.destination.url_video);

        }

    });

   }

  ngOnInit(): void {
    
  }

  next() {
    this.slickModal.slickNext();
  }
  
  prev() {
    this.slickModal.slickPrev();
  }
  
  convertContent(content){
    if(content.length > 90){
      return content.substr(0,90) + '...'
    }else{
      return content
    }
    
  }

  title(slug){
    switch(slug){
      case 'labuan-bajo':
        return 'Labuan Bajo';
      case 'mandalika':
        return 'Mandalika';
      case 'danau-toba':
        return 'Danau Toba';
      case 'likupang':
        return 'Likupang';
      case 'borobudur':
        return 'Borobudur';
      case 'solo-zoo':
        return 'Solo Zoo'
      default:
        return ''
    }
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      return num;
  }


  

}
