import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType,concatLatestFrom} from '@ngrx/effects'
import {of} from 'rxjs'
import {map, exhaustMap, catchError,switchMap,mergeMap} from 'rxjs/operators'
import {BookingService} from '../services/booking.service'
import {select, Store} from '@ngrx/store'
import * as bookingAction from '../actions/booking.actions'

@Injectable()
export class bookingEffects{
    constructor(
        private action$ : Actions,
        private bookingService : BookingService,
        private store:Store<any>
    ){}

    createBooking = createEffect(()=>
        this.action$.pipe(
            ofType(bookingAction.BookingActionTypes.CREATE_BOOKING),
            switchMap((action:any)=>
        
                this.bookingService.createBooking(action.payload).pipe(
                    map((res:any)=>{
                        return new bookingAction.createBookingSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new bookingAction.createBookingFailure(error)))
                ))
        ))
}