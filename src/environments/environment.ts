// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url:'https://uat-api.globaltix.com/api',
  url_cms:'https://frozen-chamber-80607.herokuapp.com/',
  username: 'reseller@globaltix.com',
  password:'12345',
  firebase:{
    apiKey: "AIzaSyDPKgbA4nwdvDvVRzCWvCamz4ODW1_LIqU",
    authDomain: "globaltix-4ec00.firebaseapp.com",
    databaseURL: "https://globaltix-4ec00-default-rtdb.firebaseio.com",
    projectId: "globaltix-4ec00",
    storageBucket: "globaltix-4ec00.appspot.com",
    messagingSenderId: "1053549818004",
    appId: "1:1053549818004:web:82762dde5458328dc1c481",
    measurementId: "G-X322P3HHYX"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
